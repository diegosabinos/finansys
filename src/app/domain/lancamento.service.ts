import { Lancamento } from './lancamento.model';
import { Injectable, Injector } from '@angular/core';
import { CategoriaService } from './categoria.service';
import { BaseResourceService } from '../compartilhado/servicos/base-resource.service';

import { Observable } from 'rxjs';
import { flatMap, catchError, map } from 'rxjs/operators';

import * as moment from "moment";

@Injectable({
    providedIn: 'root'
})
export class LancamentoService extends BaseResourceService<Lancamento> {

    constructor(protected injector: Injector, private categoriaService: CategoriaService) {
        super("api/lancamentos", injector, Lancamento.retorno)
    }

    salvar(lancamento: Lancamento): Observable<Lancamento> {
        return this.preencherObjeto(lancamento, super.salvar.bind(this));
    }

    atualizar(lancamento: Lancamento): Observable<Lancamento> {
        return this.preencherObjeto(lancamento, super.atualizar.bind(this));
    }

    getbyMeseAno(mes: number, ano: number): Observable<Lancamento[]> {
        return this.getAll().pipe(
            map(lancamentos => this.filtroMeseAno(lancamentos, mes, ano))
        )
    }

    private preencherObjeto(lancamento: Lancamento, funcao: any): Observable<Lancamento> {
        return this.categoriaService.getById(lancamento.categoria_id).pipe(
            flatMap(categoria => {
                lancamento.categoria = categoria;
                return funcao(lancamento);
            }),
            catchError(this.handleError)
        )
    }

    private filtroMeseAno(lancaemntos: Lancamento[], mes: number, ano: number) {
        return lancaemntos.filter(result => {
            const dataLanc = moment(result.data, "DD/MM/YYYY");
            const mesLanc = dataLanc.month() + 1 == mes;
            const anoLanc = dataLanc.year() == ano;
            if (mesLanc && anoLanc) return result;
        })
    }
}