import { BaseResourceModel } from '../compartilhado/modelos/base-resource.model';

export class Categoria extends BaseResourceModel {

    constructor(
        public id?: number,
        public nome?: string,
        public descricao?: string
    ) {
        super();
    }

    static retorno(jsonData: any): Categoria {
        return Object.assign(new Categoria(), jsonData);
    }

}
