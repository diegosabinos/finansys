import { Categoria } from './categoria.model';
import { Injectable, Injector } from '@angular/core';
import { BaseResourceService } from '../compartilhado/servicos/base-resource.service';

@Injectable({
    providedIn: 'root'
})
export class CategoriaService extends BaseResourceService<Categoria>{

    constructor(protected injector: Injector) {
        super("api/categorias", injector, Categoria.retorno);
    }

}