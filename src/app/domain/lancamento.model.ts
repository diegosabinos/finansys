import { Categoria } from './categoria.model';
import { BaseResourceModel } from '../compartilhado/modelos/base-resource.model';

export class Lancamento extends BaseResourceModel {
    constructor(
        public id?: number,
        public nome?: string,
        public descricao?: string,
        public tipo?: string,
        public valor?: string,
        public data?: string,
        public pago?: boolean,
        public categoria_id?: number,
        public categoria?: Categoria
    ) {
        super();
    }

    static tipos = {
        despesa: 'Despesa',
        receita: 'Receita'
    }

    static retorno(jsonData: any): Lancamento {
        return Object.assign(new Lancamento(), jsonData);
    }

    get pagoTexto(): string {
        return this.pago ? 'Pago' : 'Pedente';
    }
}
