import { FormControl } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-form-erro',
  template: `
    <p class="text-danger">
      {{msgError}}
    </p>
  `,
  styleUrls: ['./form-erro.component.css']
})
export class FormErroComponent implements OnInit {

  @Input('form-control') formControl: FormControl;

  constructor() { }

  ngOnInit() {
  }

  get msgError(): string | null {
    if (this.mostrarMsgError())
      return this.getMsgError();
    else
      return null;
  }

  private mostrarMsgError(): boolean {
    return this.formControl.invalid && this.formControl.touched;
  }

  private getMsgError(): string | null {
    const errors = this.formControl.errors;
    if (errors.required)
      return "Campo Obrigatório";
    else if (errors.email)
      return "Email inválido";
    else if (errors.minlength) {
      const requiredLength = errors.minlength.requiredLength;
      return `Deve ter no mínimo ${requiredLength} Caracteres`;
    }
    else if (errors.maxlength) {
      const requiredLength = errors.maxlength.requiredLength;
      return `Deve ter no máximo ${requiredLength} Caracteres`;
    }
    else
      return null;
  }

}
