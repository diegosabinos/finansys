import { Component, OnInit, Input } from '@angular/core';
import { BreadcrumbItem } from 'src/app/domain/BreadcrumbItem.interface';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {

  @Input() itens: Array<BreadcrumbItem> = [];

  constructor() { }

  ngOnInit() {
  }

  ultimoItem(item: BreadcrumbItem): boolean {
    const index = this.itens.indexOf(item);
    return index + 1 == this.itens.length;
  }

}
