import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-server-erro',
  templateUrl: './server-erro.component.html',
  styleUrls: ['./server-erro.component.css']
})
export class ServerErroComponent implements OnInit {

  @Input('server-error') msgError: string[] = null;
  constructor() { }

  ngOnInit() {
  }

}
