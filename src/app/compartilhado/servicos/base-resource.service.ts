import { BaseResourceModel } from '../modelos/base-resource.model';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Injector } from '@angular/core';

export abstract class BaseResourceService<T extends BaseResourceModel>{

    protected http: HttpClient;

    constructor(
        protected apiPath: string,
        protected injector: Injector,
        protected jsonDataToFN: (jsonData: any) => T
    ) {
        this.http = injector.get(HttpClient);
    }

    getAll(): Observable<T[]> {
        return this.http.get(this.apiPath)
            .pipe(
                map(this.listResult.bind(this)),
                catchError(this.handleError)
            )
    }

    getById(id: number): Observable<T> {
        const url = `${this.apiPath}/${id}`;
        return this.http.get(url)
            .pipe(
                map(this.result.bind(this)),
                catchError(this.handleError));
    }



    salvar(obj: T): Observable<T> {
        return this.http.post(this.apiPath, obj)
            .pipe(
                map(this.result.bind(this)),
                catchError(this.handleError));
    }

    atualizar(obj: T): Observable<T> {
        const url = `${this.apiPath}/${obj.id}`;
        return this.http.put(url, obj)
            .pipe(catchError(this.handleError),
                map(() => obj));
    }

    delete(id: number): Observable<T> {
        const url = `${this.apiPath}/${id}`;
        return this.http.delete(url)
            .pipe(catchError(this.handleError),
                map(() => null));
    }

    protected listResult(jsonData: any[]): T[] {
        const obj: T[] = [];
        jsonData.forEach(e => obj.push(this.jsonDataToFN(e)))
        return obj;
    }

    protected result(jsonData: any): T {
        return this.jsonDataToFN(jsonData);
    }

    protected handleError(error: any): Observable<any> {
        console.log("ERRO NA REQUISIÇÃO =>", error);
        return throwError(error);
    }
}