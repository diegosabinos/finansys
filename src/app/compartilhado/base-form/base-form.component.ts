import { OnInit, AfterContentChecked, Injector } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";

import { BaseResourceModel } from '../modelos/base-resource.model';
import { BaseResourceService } from '../servicos/base-resource.service';

import { switchMap } from "rxjs/operators";
import toastr from "toastr";



export abstract class BaseFormComponent<T extends BaseResourceModel> implements OnInit, AfterContentChecked {

    acaoAtual: string;
    form: FormGroup;
    tituloPagina: string;
    msgError: string[] = null;
    btnSalvar: boolean = false;

    protected rotaAtiva: ActivatedRoute;
    protected rota: Router;
    protected formBuilder: FormBuilder;

    constructor(
        protected injector: Injector,
        public obj: T,
        protected service: BaseResourceService<T>,
        protected jsonDataTo: (jsonData) => T
    ) {
        this.rotaAtiva = this.injector.get(ActivatedRoute);
        this.rota = this.injector.get(Router);
        this.formBuilder = this.injector.get(FormBuilder);
    }

    ngOnInit() {
        this.setAcaoAtual();
        this.carregarObj();
        this.buildForm();
    }

    ngAfterContentChecked() {
        this.setTituloPagina();
    }

    salvarEditar() {
        this.btnSalvar = true;

        if (this.acaoAtual == "new")
            this.salvarObj();
        else
            this.atualizarObj();
    }

    protected setAcaoAtual() {
        this.acaoAtual = this.rotaAtiva.snapshot.url[0].path == "new" ? "new" : "edit"
    }

    protected carregarObj() {
        if (this.acaoAtual == "edit") {
            this.rotaAtiva.paramMap.pipe(
                switchMap(parametros => this.service.getById(+ parametros.get("id")))
            ).subscribe(
                (result) => {
                    this.obj = result;
                    this.form.patchValue(this.obj)
                }
            )
        }
    }

    protected setTituloPagina() {
        if (this.acaoAtual == 'new')
            this.tituloPagina = this.inclusaoTitulo()
        else
            this.tituloPagina = this.editarTitulo();
    }

    protected inclusaoTitulo(): string {
        return "Novo"
    }

    protected editarTitulo(): string {
        // const nomeCategoria = this.categoria.nome || "";
        // this.tituloPagina = "Editando Categoria: " + nomeCategoria;
        return "Edição"
    }


    protected salvarObj() {
        const o: T = this.jsonDataTo(this.form.value);
        this.service.salvar(o)
            .subscribe(
                o => this.onSuccess(o),
                error => this.onError(error)
            )

    }

    protected atualizarObj() {
        const o: T = this.jsonDataTo(this.form.value);
        this.service.atualizar(o)
            .subscribe(
                o => this.onSuccess(o),
                error => this.onError(error)
            )
    }

    protected onSuccess(obj: T) {
        toastr.success("Salvo com Sucesso!");

        const baseComponentPath: string = this.rotaAtiva.snapshot.parent.url[0].path;
        this.rota.navigateByUrl(baseComponentPath, { skipLocationChange: true }).then(
            () => this.rota.navigate([baseComponentPath, obj.id, "edit"])
        )
    }

    protected onError(error) {
        toastr.error("Ocorreu um erro ao Salvar/Editar");
        this.btnSalvar = false;
        if (error.status === 422)
            this.msgError = JSON.parse(error._body).errors;
        else
            this.msgError = ["Falha na comunicação com o servidor. Por Favor, tente mais tarde."];
    }

    protected abstract buildForm(): void;

}
