import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";
import { BreadcrumbComponent } from './componentes/breadcrumb/breadcrumb.component';
import { HeaderComponent } from './componentes/header/header.component';
import { FormErroComponent } from './componentes/form-erro/form-erro.component';
import { ServerErroComponent } from './componentes/server-erro/server-erro.component';

@NgModule({
  declarations: [
    BreadcrumbComponent,
    HeaderComponent,
    FormErroComponent,
    ServerErroComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    BreadcrumbComponent,
    HeaderComponent,
    FormErroComponent,
    ServerErroComponent
  ]
})
export class CompartilhadoModule { }
