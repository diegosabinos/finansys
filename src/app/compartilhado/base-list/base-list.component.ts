import { OnInit } from '@angular/core';
import { BaseResourceModel } from '../modelos/base-resource.model';
import { BaseResourceService } from '../servicos/base-resource.service';

export abstract class BaseListComponent<T extends BaseResourceModel> implements OnInit {

    listObj: T[] = [];

    constructor(protected service: BaseResourceService<T>) { }

    ngOnInit() {
        this.service.getAll().subscribe(
            result => this.listObj = result.sort((a, b) => b.id - a.id),
            error => alert('Error ao Carregar a lista')
        )
    }

    delete(obj: T) {
        const confirmacao = confirm("Deseja realmente excluir esta Lancamento?");

        if (confirmacao) {
            this.service.delete(obj.id).subscribe(
                () => this.listObj = this.listObj.filter(el => el != obj),
                () => alert("Erro ao Tentar Excluir")
            )
        }

    }

}
