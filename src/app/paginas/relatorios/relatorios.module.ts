import { CompartilhadoModule } from './../../compartilhado/compartilhado.module';
import { NgModule } from '@angular/core';

import { RelatoriosRoutingModule } from './relatorios-routing.module';
import { RelatorioFormComponent } from './relatorio-form/relatorio-form.component';
import { ChartModule } from "primeng/chart";

@NgModule({
  declarations: [
    RelatorioFormComponent
  ],
  imports: [
    CompartilhadoModule,
    RelatoriosRoutingModule,
    ChartModule
  ]
})
export class RelatoriosModule { }
