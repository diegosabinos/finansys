import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { LancamentoService } from './../../../domain/lancamento.service';
import { Lancamento } from './../../../domain/lancamento.model';
import { CategoriaService } from './../../../domain/categoria.service';
import { Categoria } from './../../../domain/categoria.model';

import currencyFormatter from "currency-formatter";

@Component({
  selector: 'app-relatorio-form',
  templateUrl: './relatorio-form.component.html',
  styleUrls: ['./relatorio-form.component.css']
})
export class RelatorioFormComponent implements OnInit {

  despesaTotal: any = 0;
  receitaTotal: any = 0;
  saldoTotal: any = 0;

  despesaDataGrafico: any;
  receitaDataGrafico: any;

  graficoOptions = {
    scales: {
      yAxes: [{ ticks: { beginAtZero: true } }]
    }
  };

  categorias: Categoria[] = [];
  lancamentos: Lancamento[] = [];

  @ViewChild('mes') mes: ElementRef = null;
  @ViewChild('ano') ano: ElementRef = null;

  constructor(private lancamentoService: LancamentoService, private categoriaService: CategoriaService) { }

  ngOnInit() {
    this.categoriaService.getAll().subscribe(result => this.categorias = result)
  }


  gerarRelatorio() {
    const mes = this.mes.nativeElement.value;
    const ano = this.ano.nativeElement.value;

    if (!mes || !ano)
      alert("Necessário escolher Mês e Ano para gerar o Relatório")
    else
      this.lancamentoService.getbyMeseAno(mes, ano).subscribe(this.setValues.bind(this))
  }

  private setValues(lancamentos: Lancamento[]) {
    this.lancamentos = lancamentos;
    this.calcularSaldo();
    this.carregarGrafico();
  }

  private calcularSaldo() {
    let despesa = 0;
    let receita = 0;

    this.lancamentos.forEach(lancamento => {
      if (lancamento.tipo == 'receita')
        receita += currencyFormatter.unformat(lancamento.valor, { code: 'BRL' })
      else
        despesa += currencyFormatter.unformat(lancamento.valor, { code: 'BRL' })
    });

    this.despesaTotal = currencyFormatter.format(despesa, { code: 'BRL' });
    this.receitaTotal = currencyFormatter.format(receita, { code: 'BRL' });
    this.saldoTotal = currencyFormatter.format(receita - despesa, { code: 'BRL' });

  }

  private carregarGrafico() {
    this.receitaDataGrafico = this.obterValoresGrafico('receita', 'Gráfico de Receitas', '#9CCC65');
    this.despesaDataGrafico = this.obterValoresGrafico('despesa', 'Gráfico de Despesas', '#E03131');
  }

  private obterValoresGrafico(tipoLancamento: string, titulo: string, cor: string) {
    const graficoData = [];
    this.categorias.forEach(categoria => {
      const filtroLancamento = this.lancamentos.filter(
        lancamento => (lancamento.categoria_id == categoria.id) && (lancamento.tipo == tipoLancamento)
      );
      if (filtroLancamento.length > 0) {
        const total = filtroLancamento.reduce(
          (total, lancamento) => total += currencyFormatter.unformat(lancamento.valor, { code: 'BRL' }), 0
        )
        graficoData.push({
          nomeCategoria: categoria.nome,
          saldoTotal: total
        })
      }
    });

    return {
      labels: graficoData.map(item => item.nomeCategoria),
      datasets: [{
        label: titulo,
        backgroundColor: cor,
        data: graficoData.map(item => item.saldoTotal)
      }]
    }
  }

}
