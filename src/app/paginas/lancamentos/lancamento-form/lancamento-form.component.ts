import { BaseFormComponent } from 'src/app/compartilhado/base-form/base-form.component';
import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";

import { Lancamento } from '../../../domain/lancamento.model';
import { LancamentoService } from '../../../domain/lancamento.service';

import { Categoria } from 'src/app/domain/categoria.model';
import { CategoriaService } from 'src/app/domain/categoria.service';

@Component({
  selector: 'app-lancamento-form',
  templateUrl: './lancamento-form.component.html',
  styleUrls: ['./lancamento-form.component.css']
})
export class LancamentoFormComponent extends BaseFormComponent<Lancamento> implements OnInit {

  categorias: Array<Categoria> = [];

  imaskConfig = {
    mask: Number,
    scale: 2,
    thousandsSeparator: '',
    padFractionalZeros: true,
    normalizeZeros: true,
    radix: ','
  };

  ptBR = {
    firstDayOfWeek: 0,
    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
    dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
    monthNames: [
      'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
      'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'
    ],
    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    today: 'Hoje',
    clear: 'Limpar'
  }

  constructor(
    protected lancamentoService: LancamentoService,
    protected categoriaService: CategoriaService,
    protected injector: Injector
  ) {
    super(injector, new Lancamento, lancamentoService, Lancamento.retorno);
  }

  ngOnInit() {
    this.carregarCategorias();
    super.ngOnInit();
  }

  get tipos(): Array<any> {
    return Object.entries(Lancamento.tipos).map(
      ([value, text]) => {
        return {
          text: text,
          value: value
        }
      }
    )
  }

  protected buildForm() {
    this.form = this.formBuilder.group({
      id: [null],
      nome: [null, [Validators.required, Validators.minLength(2)]],
      descricao: [null],
      tipo: ["despesa", [Validators.required]],
      valor: [null, [Validators.required]],
      data: [null, [Validators.required]],
      pago: [true, [Validators.required]],
      categoria_id: ["", [Validators.required]]
    })
  }

  protected carregarCategorias() {
    this.categoriaService.getAll().subscribe(
      categorias => this.categorias = categorias
    );
  }

  protected inclusaoTitulo(): string {
    return "Cadastro de Novo Lançamento";
  }

  protected editarTitulo(): string {
    const nomeLancamento = this.obj.nome || "";
    return "Editando Lançamento: " + nomeLancamento;
  }
}
