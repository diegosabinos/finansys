import { BaseListComponent } from 'src/app/compartilhado/base-list/base-list.component';
import { Lancamento } from '../../../domain/lancamento.model';
import { LancamentoService } from '../../../domain/lancamento.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-lancamento-list',
  templateUrl: './lancamento-list.component.html',
  styleUrls: ['./lancamento-list.component.css']
})
export class LancamentosListComponent extends BaseListComponent<Lancamento> {

  constructor(private lancamentoService: LancamentoService) {
    super(lancamentoService);
  }

}
