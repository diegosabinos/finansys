import { NgModule } from '@angular/core';

import { LancamentosRoutingModule } from './lancamentos-routing.module';

import { LancamentosListComponent } from './lancamento-list/lancamento-list.component';
import { LancamentoFormComponent } from './lancamento-form/lancamento-form.component';

import { CalendarModule } from "primeng/calendar";
import { IMaskModule } from "angular-imask";
import { CompartilhadoModule } from 'src/app/compartilhado/compartilhado.module';

@NgModule({
  declarations: [
    LancamentosListComponent,
    LancamentoFormComponent
  ],
  imports: [
    CompartilhadoModule,
    LancamentosRoutingModule,
    CalendarModule,
    IMaskModule

  ]
})
export class LancamentosModule { }
