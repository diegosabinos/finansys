import { Categoria } from './../../../domain/categoria.model';
import { CategoriaService } from './../../../domain/categoria.service';
import { Component } from '@angular/core';
import { BaseListComponent } from 'src/app/compartilhado/base-list/base-list.component';

@Component({
  selector: 'app-categoria-list',
  templateUrl: './categoria-list.component.html',
  styleUrls: ['./categoria-list.component.css']
})
export class CategoriasListComponent extends BaseListComponent<Categoria> {

  constructor(private categoriaService: CategoriaService) {
    super(categoriaService)
  }
}
