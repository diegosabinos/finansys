import { Component, Injector } from '@angular/core';
import { Validators } from "@angular/forms";

import { Categoria } from './../../../domain/categoria.model';
import { CategoriaService } from './../../../domain/categoria.service';
import { BaseFormComponent } from 'src/app/compartilhado/base-form/base-form.component';

@Component({
  selector: 'app-categoria-form',
  templateUrl: './categoria-form.component.html',
  styleUrls: ['./categoria-form.component.css']
})
export class CategoriaFormComponent extends BaseFormComponent<Categoria> {

  constructor(
    protected categoriaService: CategoriaService,
    protected injector: Injector
  ) {
    super(injector, new Categoria(), categoriaService, Categoria.retorno);
  }

  protected buildForm() {
    this.form = this.formBuilder.group({
      id: [null],
      nome: [null, [Validators.required, Validators.minLength(2)]],
      descricao: [null]
    })
  }

  protected inclusaoTitulo(): string {
    return "Cadastro de Nova Categoria";
  }

  protected editarTitulo(): string {
    const nomeCategoria = this.obj.nome || "";
    return "Editando Categoria: " + nomeCategoria;
  }
}
