import { NgModule } from '@angular/core';

import { CompartilhadoModule } from 'src/app/compartilhado/compartilhado.module';

import { CategoriasRoutingModule } from './categorias-routing.module';
import { CategoriasListComponent } from './categoria-list/categoria-list.component';
import { CategoriaFormComponent } from './categoria-form/categoria-form.component';

@NgModule({
  declarations: [
    CategoriasListComponent,
    CategoriaFormComponent
  ],
  imports: [
    CompartilhadoModule,
    CategoriasRoutingModule,
  ]
})
export class CategoriasModule { }
