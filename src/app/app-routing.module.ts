import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'categoria', loadChildren: './paginas/categoria/categorias.module#CategoriasModule' },
  { path: 'lancamento', loadChildren: './paginas/lancamentos/lancamentos.module#LancamentosModule' },
  { path: 'relatorio', loadChildren: './paginas/relatorios/relatorios.module#RelatoriosModule' },
  { path: '', redirectTo: '/relatorio', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
